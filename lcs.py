import datetime

# ~ ~ ~ The Longest Common Subsequence algorithm ~ ~ ~

# ----------------------------------------------------
# Programação Dinâmica
# ----------------------------------------------------

# Uncomment ini and finish time and the operations var to get these values
def lcs_dynamic(X, Y):
    # ini = datetime.datetime.now()
    # operations = 0

    # find the length of the strings 
    m = len(X) 
    n = len(Y) 
  
    # declaring the array for storing the dp values 
    L = [[None]*(n + 1) for i in range(m + 1)] 
  
    """Following steps build L[m + 1][n + 1] in bottom up fashion 
    Note: L[i][j] contains length of LCS of X[0..i-1] 
    and Y[0..j-1]"""
    for i in range(m + 1): 
        for j in range(n + 1): 
            if i == 0 or j == 0 : 
                L[i][j] = 0
                # operations += 1
            elif X[i-1] == Y[j-1]: 
                L[i][j] = L[i-1][j-1]+1
                # operations += 1
            else: 
                L[i][j] = max(L[i-1][j], L[i][j-1])
                # operations += 1

    # finish = datetime.datetime.now()

    # Uncomment to show n*m and num operations
    # print('{:10}{:19}{:-4d}'.format((str(m) + '*' + str(n)), str(finish - ini), operations))

    # Uncomment to print result matrix
    # print('\n', *L, sep='\n')
  
    # L[m][n] contains the length of LCS of X[0..n-1] & Y[0..m-1] 
    return L[m][n] 
# end of function lcs
# # Driver program to test the above function 
# X = "AGGTABHDT"
# Y = "GXTXAYB"
# # print ("Length of LCS is ", lcs_dynamic(X, Y)) 

# This fujnction runs the lcsdynamic() many times for a list of tuples of strings
# and get time execution
def run_lcs_dynamic():
    lista = [   ('a', 'a'), 
                ('aa', 'aa'), 
                ('aaa', 'aaa'), ('a', 'aaaaaaaaa'), 
                ('aaaa', 'aaaa'), ('aa', 'aaaaaaaa'), 
                ('ababa', 'babab'), 
                ('abcdef', 'defgei'), ('ab', 'ababababababababab'), 
                ('wajshth', 'ajdhuwa'), 
                ('shagshgd', 'gshajsgd'), ('abab', 'abababababababab'),
                ('hsjashdty', 'ajklsdfhj'), 
                ('asdfafbnmh', 'sdfgsdfgnm'), ('abab', 'ababababababababababababa')]
    print('\nn*m       Tempo exec         Num operations')
    print('-------------------------------------------')
    for item in lista:
        # nm = len(item[0]) * len(item[1])
        # print('n*m =', nm)
        num_repetitions = 10000
        ini = datetime.datetime.now()

        for i in range(num_repetitions):
            lcs_dynamic(item[0], item[1])

        finish = datetime.datetime.now()

        print('Tempo de exec - item {}: {}'.format(str(len(item[0])) + '*' + str(len(item[1])), str((finish - ini) / num_repetitions)))

# run_lcs_dynamic()  # ------------------- run the dynamic program uncomment this line 

# Link References:
# https://www.geeksforgeeks.org/python-program-for-longest-common-subsequence/ (code is contributed by Nikhil Kumar Singh(nickzuck_007))


# ----------------------------------------------------
# Programação Recursiva
# ----------------------------------------------------
# https://www.geeksforgeeks.org/python-program-for-longest-common-subsequence/

# A Naive recursive Python implementation of LCS problem
operations = 0
def lcs_recursive(X, Y, m, n): 
  
    global operations    

    if m == 0 or n == 0:
        operations += 1
        return 0
    elif X[m-1] == Y[n-1]:
        operations += 1
        return 1 + lcs_recursive(X, Y, m-1, n-1)
    else:
        operations += 1
        return max(lcs_recursive(X, Y, m, n-1), lcs_recursive(X, Y, m-1, n))
  
# Driver program to test the above function 
# X = "AAAAAAAAAAAAAAAA"
# Y = "BBBBBBBBBBBBBBBB"

# init = datetime.datetime.now()
# print ("\nLength of LCS is ", lcs_recursive(X, Y, len(X), len(Y)))
# fini = datetime.datetime.now()
# time = fini - init

# print('Num de operações:', operations)
# print('Tempo de execução:', time)

def run_lcs_recursive():
    global operations
    lista = [   ('a', 'b', 1, 1), 
                ('aa', 'bb', 2, 2), 
                ('aaa', 'bbb', 3, 3), ('a', 'bbbbbbbbb', 1, 9), 
                ('aaaa', 'bbbb', 4, 4), ('aa', 'bbbbbbbb', 2, 8), 
                ('aaaaa', 'bbbbb', 5, 5),
                ('aaaaaa', 'bbbbbb', 6, 6), ('aa', 'bbbbbbbbbbbbbbbbbb', 2, 18), 
                ('aaaaaaa', 'bbbbbbb', 7, 7), 
                ('aaaaaaaa', 'bbbbbbbb', 8, 8), ('aaaa', 'bbbbbbbbbbbbbbbb', 4, 16),
                ('aaaaaaaaa', 'bbbbbbbbb', 9, 9), 
                ('aaaaaaaaaa', 'bbbbbbbbbb', 10, 10), ('aaaa', 'bbbbbbbbbbbbbbbbbbbbbbbbb', 4, 25),
                ('aaaaaaaaaaa', 'bbbbbbbbbbb', 11, 11),
                ('aaaaaaaaaaaa', 'bbbbbbbbbbbb', 12, 12), ('aaaaaaaa', 'bbbbbbbbbbbbbbbbbb', 8, 18),
                ('aaaaaaaaaaaaa', 'bbbbbbbbbbbbb', 13, 13),
                ('aaaaaaaaaaaaaa', 'bbbbbbbbbbbbbb', 14, 14), ('aaaaaaa', 'bbbbbbbbbbbbbbbbbbbbbbbbbbbb', 7, 28)]

    print('\nn*m       Tempo exec         Num operations')
    print('-------------------------------------------')
    for item in lista:
        # nm = len(item[0]) * len(item[1])
        # print('n*m =', nm)
        num_repetitions = 1
        ini = datetime.datetime.now()

        for i in range(num_repetitions):
            operations = 0
            lcs_recursive(item[0], item[1], item[2], item[3])

        finish = datetime.datetime.now()

        print('{:10}{:19}{}'.format(str(len(item[0])) + '*' + str(len(item[1])), str((finish - ini) / num_repetitions), operations))

# run_lcs_recursive()


# ----------------------------------------------------
# Programação Dinâmica com Memoization
# ----------------------------------------------------
# https://www.geeksforgeeks.org/longest-common-subsequence-dp-using-memoization/

# Python3 program to memoize 
# recursive implementation of LCS problem 
maximum = 1000

# Returns length of LCS for X[0..m-1], Y[0..n-1] */ 
# memoization applied in recursive solution
num_operations = 0
def lcs_memo(X, Y, m, n, dp): 
    global num_operations
    # base case 
    if (m == 0 or n == 0):
        num_operations += 1
        return 0

    # if the same state has already been computed 
    if (dp[m - 1][n - 1] != -1):
        num_operations += 1
        return dp[m - 1][n - 1] 

    # if equal, then we store the value of the function call 
    if (X[m - 1] == Y[n - 1]): 
        # store it in arr to avoid further repetitive work in future function calls 
        dp[m - 1][n - 1] = 1 + lcs_memo(X, Y, m - 1, n - 1, dp)
        num_operations += 1
        return dp[m - 1][n - 1] 

    else: 
        # store it in arr to avoid further repetitive work in future function calls 
        dp[m - 1][n - 1] = max(lcs_memo(X, Y, m, n - 1, dp), lcs_memo(X, Y, m - 1, n, dp)) 
        num_operations += 1
        return dp[m - 1][n - 1] 

# Driver Code 
# X = "aaaaaaaa"
# Y = "bbbbbbbb"
# m = len(X) 
# n = len(Y) 

# dp = [[-1 for i in range(maximum)] for i in range(m)] 

# ini = datetime.datetime.now()
# print("Length of LCS Memo:", lcs_memo(X, Y, m, n, dp))
# finish = datetime.datetime.now()
# print('\nTempo exdecucao:', finish - ini)
# print('Num operations:', num_operations)
# This code is contributed by Mohit Kumar 

def run_lcs_memo():
    global num_operations
    lista = [   ('a', 'b', 1, 1), 
                ('aa', 'bb', 2, 2), 
                ('aaa', 'bbb', 3, 3), ('a', 'bbbbbbbbb', 1, 9), 
                ('aaaa', 'bbbb', 4, 4), ('aa', 'bbbbbbbb', 2, 8), 
                ('aaaaa', 'bbbbb', 5, 5),
                ('aaaaaa', 'bbbbbb', 6, 6), ('aa', 'bbbbbbbbbbbbbbbbbb', 2, 18), 
                ('aaaaaaa', 'bbbbbbb', 7, 7), 
                ('aaaaaaaa', 'bbbbbbbb', 8, 8), ('aaaa', 'bbbbbbbbbbbbbbbb', 4, 16),
                ('aaaaaaaaa', 'bbbbbbbbb', 9, 9), 
                ('aaaaaaaaaa', 'bbbbbbbbbb', 10, 10), ('aaaa', 'bbbbbbbbbbbbbbbbbbbbbbbbb', 4, 25),
                ('aaaaaaaaaaa', 'bbbbbbbbbbb', 11, 11),
                ('aaaaaaaaaaaa', 'bbbbbbbbbbbb', 12, 12), ('aaaaaaaa', 'bbbbbbbbbbbbbbbbbb', 8, 18),
                ('aaaaaaaaaaaaa', 'bbbbbbbbbbbbb', 13, 13),
                ('aaaaaaaaaaaaaa', 'bbbbbbbbbbbbbb', 14, 14), ('aaaaaaa', 'bbbbbbbbbbbbbbbbbbbbbbbbbbbb', 7, 28),
                ('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb', 64, 64),]

    print('\nn*m       Tempo exec         Num operations')
    print('-------------------------------------------')
    for item in lista:
        # nm = len(item[0]) * len(item[1])
        # print('n*m =', nm)
        num_repetitions = 30
        ini = datetime.datetime.now()

        for i in range(num_repetitions):

            num_operations = 0
            dp = [[-1 for i in range(maximum)] for i in range(item[2])] 
            lcs_memo(item[0], item[1], item[2], item[3], dp)

        finish = datetime.datetime.now()

        print('{:10}{:19}{}'.format(str(len(item[0])) + '*' + str(len(item[1])), str((finish - ini) / num_repetitions), num_operations))
run_lcs_memo()


'''
-----------------------------------------------
Programação Recursiva com Memoization
-----------------------------------------------

n*m       Tempo exec         Num operations
-------------------------------------------
1*1       0:00:00.000035     3
2*2       0:00:00.000082     9
3*3       0:00:00.000115     19
1*9       0:00:00.000051     19
4*4       0:00:00.000296     33
2*8       0:00:00.000232     33
5*5       0:00:00.000364     51
6*6       0:00:00.000265     73
2*18      0:00:00.000215     73
7*7       0:00:00.000430     99
8*8       0:00:00.000563     129
4*16      0:00:00.000216     129
9*9       0:00:00.000463     163
10*10     0:00:00.001042     201
4*25      0:00:00.000465     201
11*11     0:00:00.000577     243
12*12     0:00:00.000942     289
8*18      0:00:00.000628     289
13*13     0:00:00.000711     339
14*14     0:00:00.001025     393
7*28      0:00:00.001141     393
64*64     0:00:00.008581     8193


Versão sem dados de tamanho diferentes

n*m       Tempo exec         Num operations
-------------------------------------------
1*1       0:00:00.000035     3
2*2       0:00:00.000082     9
3*3       0:00:00.000115     19
4*4       0:00:00.000296     33
5*5       0:00:00.000364     51
6*6       0:00:00.000265     73
7*7       0:00:00.000430     99
8*8       0:00:00.000563     129
9*9       0:00:00.000463     163
10*10     0:00:00.001042     201
11*11     0:00:00.000577     243
12*12     0:00:00.000942     289
13*13     0:00:00.000711     339
14*14     0:00:00.001025     393
64*64     0:00:00.008581     8193
'''

'''
-----------------------------------------------
Programação Recursiva
-----------------------------------------------
               
n*m       Tempo exec         Num operations
-------------------------------------------
1*1       0:00:00.000002     3
2*2       0:00:00.000007     11
3*3       0:00:00.000027     39
1*9       0:00:00.000012     19
4*4       0:00:00.000094     139
2*8       0:00:00.000057     89
5*5       0:00:00.000459     503
6*6       0:00:00.001106     1847
2*18      0:00:00.000208     379
7*7       0:00:00.003244     6863
8*8       0:00:00.010607     25739
4*16      0:00:00.004179     9689
9*9       0:00:00.039253     97239
10*10     0:00:00.148426     369511
4*25      0:00:00.017905     47501
11*11     0:00:00.744986     1410863
12*12     0:00:02.250390     5408311
8*18      0:00:01.293034     3124549
13*13     0:00:07.701138     20801199
14*14     0:00:29.330457     80233199 ----- começa a demorar bastante com 14*14
7*28      0:00:04.895515     13449039
15*15     0:02:06.074160     310235039
16*16     0:08:00.635719     1202160779

'''

# Analisando LCS rec
# def lcs_recursive(X, Y, m, n): 
  
#     global operations    

#     if m == 0 or n == 0:
#         return 0 ------------------------------------------------------------------ 1
#     elif X[m-1] == Y[n-1]:
#         return 1 + lcs_recursive(X, Y, m-1, n-1) ---------------------------------- 1
#     else:
#         return max(lcs_recursive(X, Y, m, n-1), lcs_recursive(X, Y, m-1, n)) ------ 1


'''
-----------------------------------------------
Programação Dinâmica
-----------------------------------------------

# Análise da complexidade:
- Se strings iguais: O(n^2)
- Se strings diferentes: O(n*m)

# Tempo de execução e número de operações

n*m       Tempo exec         Num operations
-------------------------------------------
1*1       0:00:00.000003     4
2*2       0:00:00.000006     9
3*3       0:00:00.000007     16
1*9       0:00:00.000008     20
4*4       0:00:00.000009     25
2*8       0:00:00.000009     27
5*5       0:00:00.000021     36
6*6       0:00:00.000028     49
2*16      0:00:00.000022     51
7*7       0:00:00.000040     64
8*8       0:00:00.000045     81
4*16      0:00:00.000043     85
9*9       0:00:00.000055     100
10*10     0:00:00.000062     121
4*25      0:00:00.000054     130
(O tempo de execução como é infinitamente pequeno, dada o tamanho das strings, foram obtidos através de 10000 execuções e posteriormente dividido por estes nºs de repetições)

# Tempo estimado para instancias de maior dimensão
Como o algoritmo é quadrático, o tempo de execução será aproximadamente multiplicado por 4 cada vez que a string dobra de tamanho.

Para uma entrada de 16*16 = 4 * 0:00:00.000045 (tempo de 8*8) = 0:00:00.00018
Então, utilizando uma regra de 3, podemos calcular o tempo para, por exemplo, 64*64 = 0:00:00.00288

No intuito de confirmar este valor obtivo, foram realizadas algumas execuções para o valor de 64*64, resultando nas seguintes soluções:
- exec 1: 0:00:00.001966
- exec 2: 0:00:00.001815
- exec 3: 0:00:00.002121
- exec 4: 0:00:00.002091
- exec 5: 0:00:00.001898


Exemplo de matrix final para as strings ('asdfafbnmh', 'sdfgsdfgnm'):
[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
[0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
[0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2]
[0, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3]
[0, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3]
[0, 1, 2, 3, 3, 3, 3, 4, 4, 4, 4]
[0, 1, 2, 3, 3, 3, 3, 4, 4, 4, 4]
[0, 1, 2, 3, 3, 3, 3, 4, 4, 5, 5]
[0, 1, 2, 3, 3, 3, 3, 4, 4, 5, 6]
[0, 1, 2, 3, 3, 3, 3, 4, 4, 5, 6]

Tempo exec
-------------------------------------------
Tempo de exec - item 1*1: 0:00:00.000003
Tempo de exec - item 2*2: 0:00:00.000006
Tempo de exec - item 3*3: 0:00:00.000007
Tempo de exec - item 1*9: 0:00:00.000008
Tempo de exec - item 4*4: 0:00:00.000009
Tempo de exec - item 2*8: 0:00:00.000009
Tempo de exec - item 5*5: 0:00:00.000021
Tempo de exec - item 6*6: 0:00:00.000028
Tempo de exec - item 2*16: 0:00:00.000022
Tempo de exec - item 7*7: 0:00:00.000040
Tempo de exec - item 8*8: 0:00:00.000045
Tempo de exec - item 4*16: 0:00:00.000043
Tempo de exec - item 9*9: 0:00:00.000055
Tempo de exec - item 10*10: 0:00:00.000062
Tempo de exec - item 4*25: 0:00:00.000054
'''