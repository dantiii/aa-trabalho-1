# Algoritmos Avançados | Trabalho 1 - Estratégias de Desenvolvimento de Algoritmos

Docente: Joaquim Madeira

Aluno: Dante Marinho (83672)

## Problema nº 11 - Dadas duas cadeias de caracteres, determinar a sua subsequência comum mais longa ("The Longest Common Subsequence Problem").

## Como Correr o Programa

Chamar o arquivo lcs.py:

python lcs.py

## Notas sobre o código

Para cada tipo de algoritmo, foi desenvolvido uma função. Logo abaixo da função existe um bloco de código comentado que serve para a realização de testes para apenas uma tiragem. A seguir ao código comentado, existe uma função auxiliar, com o nome a começar por "run_", que serve para correr o código várias tiragens de uma só vez, que possivelmente também poderá estar comentada.