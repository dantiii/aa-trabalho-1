import datetime

# Este ficheiro de script serve para realização de teste.

# t = datetime.datetime(2020, 1, 1, 0, 0, 0, 147456)

# Obter a progressão de tempo de execução: para cada valor de entrada dobrado o tempo de execução multiplica por 4
t = datetime.timedelta(microseconds=147456)
num = 512

print('{:8} --- {}'.format(num, t))
for i in range(11):
    t = t / 4
    num = num / 2
    print('{:8} --- {}'.format(num, t))


print('Tempo:', t)

'''
Tamanho string      Tempo execução
            4*4             0:00:00.000009
            8*8             0:00:00.000036
        16*16               0:00:00.000144
        32*32               0:00:00.000576
        64*64               0:00:00.002304
        128*128             0:00:00.009216
        256*256             0:00:00.036864
        512*512             0:00:00.147456
    1024*1024               0:00:00.589824
    2048*2048               0:00:02.359296
    4096*4096               0:00:09.437184
    8192*8192               0:00:37.748736
    16384*16384             0:02:30.994944
    32768*32768             0:10:03.979776
    65536*65536             0:40:15.919104
    131072*131072           2:41:03.676416
    262144*262144 ---       10:44:14.705664
  '''